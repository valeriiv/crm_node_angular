const moment = require('moment');
const Order = require('../models/Order');

const errorHandler = require('../utils/errorHandler');

module.exports.overview = async (req, resp, next) => {
    try {
        const allOrders = await Order.find({
            user: req.user.id
        }).sort({date: 1});

        const orderMap = getOrderMap(allOrders);

        const yesterdayOrders = orderMap[moment().add(-1, 'd').format('DD.MM.YYYY')] || [];

        //quantity of orders by yesterday
        const yesterdayOrdersNumber = yesterdayOrders.length;

        //quantity of orders
        const totalOrdersNumber = allOrders.length;

        //total days
        const daysNumber = Object.keys(orderMap).length;

        //total orders for a day
        const ordersPerDay = (totalOrdersNumber / daysNumber).toFixed(0);

        //percent for orders quantity
        const ordersPercent = (((yesterdayOrders / ordersPerDay) - 1) * 1000).toFixed(2);

        //total revenue
        const totalGain = calculatePrice(allOrders);

        //revenue for a day
        const gainPerday = totalGain / daysNumber;

        //revenue for yesterday
        const yesterdayGain = calculatePrice(yesterdayOrders);

        //percent of revenue
        const gainPercent = (((yesterdayGain / gainPerday) - 1) * 1000).toFixed(2);

        //compare of revenue
        const compareGain = (yesterdayGain - gainPerday).toFixed(2);

        //compare quantity of orders
        const compareNumber = (yesterdayOrdersNumber - ordersPerDay).toFixed(2);

        resp.status(200).json({
            gain: {
                percent: Math.abs(+gainPercent),
                compare: Math.abs(+compareGain),
                yesterday: +yesterdayGain,
                isHigher: gainPercent > 0
            },
            orders: {
                percent: Math.abs(+ordersPercent),
                compare: Math.abs(+compareNumber),
                yesterday: +yesterdayOrdersNumber,
                isHigher: +ordersPercent > 0
            }
        });
    }
    catch (err) {
        errorHandler(resp, err);
    }
};

module.exports.analytics = async (req, resp, next) => {
    try {
        const allOrders = await Order.find({
            user: req.user.id
        }).sort({date: 1});

        const ordersMap = getOrderMap(allOrders);

        const average = +(calculatePrice(allOrders) / Object.keys(ordersMap).length).toFixed(2);

        const chart = Object.keys(ordersMap).map(label => {
            const gain = calculatePrice(ordersMap[label]);

            const order = ordersMap[label].length;

            return {
                label,
                order,
                gain
            };
        });

        resp.status(200).json({
            average, chart
        });
    }
    catch(err) {
        errorHandler(resp, err);
    }
};

function getOrderMap(orders = []) {
    const daysOrders = {};

    orders.forEach(order => {
        const date = moment(order.date).format('DD.MM.YYYY');

        if (date === moment.format('')) {
            return;
        }

        if (!daysOrders[date]) {
            daysOrders[date] = [];
        }

        daysOrders[date].push(order);
    });

    return daysOrders;
}

function calculatePrice(orders = []) {
    return orders.reduce((total, order) => {
        const orderPrice = order.list.reduce((orderTotal, item) => {
            return orderTotal += item.cost * item.quantity;
        }, 0);
        return total += orderPrice;
    }, 0);
}