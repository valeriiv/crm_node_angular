const Order = require('../models/Order');

const errorHandler = require('../utils/errorHandler');

module.exports.getAll = async (req, resp, next) => {
    const query = {
        user: req.user.id
    };

    if (req.user.start) {
        query.date = {
            $gte: req.query.start
        };
    }

    if (req.query.end) {
        if (!query.date) {
            query.date = {};
        }

        query.date['$lte'] = req.query.end;
    }

    if (req.query.order) {
        query.order = +req.query.order;
    }

    try {
        const orders = await Order
        .find(query)
        .sort({ date: -1 })
        .skip(+req.query.offset)
        .limit(+req.query.limit);

        resp.status(200).json(orders);
    } catch (error) {
        errorHandler(resp, error);
    }
};

module.exports.create = async (req, resp, next) => {
    try {
        const lastOrder = await Order.findOne({ user: req.user.id })
        .sort({ data: -1 });

        const maxOrder = lastOrder ? lastOrder.order : 0;

        const order = await new Order({
            list: req.body.list,
            user: req.user.id,
            order: maxOrder + 1
        }).save();

        resp.status(201).json(order);
    } catch (error) {
        errorHandler(resp, error);
    }
};
