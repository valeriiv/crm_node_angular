const bcrypt = require('bcryptjs');

const jwt = require('jsonwebtoken');

const { secret } = require("../config");

const User = require('../models/User');

const errorHandler = require('../utils/errorHandler');

module.exports.login = async (req, resp, next) => {
    const candidate = await User.findOne({
        email: req.body.email
    });

    if (candidate) {
        const passwordResult = bcrypt.compareSync(req.body.password, candidate.password);

        if (passwordResult) {
            const token = jwt.sign({
                email: candidate.email,
                userId: candidate._id
            }, secret, { expiresIn: 60 * 60 });

            resp.status(200).json({ token: `Bearer ${token}` });
        }

    }
    else {
        resp.status(401).json({ message: 'Пароли не совпадают. попробуйте снова' });
    }
};

module.exports.register = async (req, resp, next) => {
    const candidate = await User.findOne({
        email: req.body.email
    });

    if (candidate) {
        resp.status(409).json({ message: 'Такой email уже занят. используйте другой.' })
    }
    else {
        const salt = bcrypt.genSaltSync(10);

        const password = req.body.password;

        const user = new User({
            email: req.body.email,
            password: bcrypt.hashSync(password, salt)
        });

        try {
            await user.save();

            resp.status(201).json(user);
        } catch (error) {
            errorHandler(resp, error);
        }
    }
};